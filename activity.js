// Objective 1
//Find the users with letter "s" in their first name or "d" in their last name. use the $or operator, show only the firstName and lastName fields and hide the _id field.

db.users.find({$or: [{firstName: {$regex:"S", $options: "$i"}}, {lastName: {$regex: "D", $options: "$i"}}]}, {age: 0, contact: 0, courses: 0, department: 0, _id: 0});

//Objective 2
//find users who are from the HR Department and their age is greater than or equal to 70., use the $and operator

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});

//find users with the letter e in their first name and has an age of less than or equal to 30., use the $and , $regex and $lte operators.

db.users.find({$and:[{firstName: {$regex:"E",$options: "$i"}},{age: {$lte: 30}}]});
